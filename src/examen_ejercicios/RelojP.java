package examen_ejercicios;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;



public class RelojP extends javax.swing.JFrame implements Runnable{
    String h, m, s, xm;
    Calendar reloj;
    Thread hi1o;

    public RelojP(){
        initComponents();
        hi1o = new Thread(this);
        hi1o.start();
        
        setLocationRelativeTo(null);
        setTitle("Reloj");
        setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("00:00:00 XX");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(75, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(74, 74, 74))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(jLabel1)
                .addContainerGap(76, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RelojP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RelojP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RelojP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RelojP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RelojP().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {
        Thread actual = Thread.currentThread();
        
        while(actual==hi1o){
            tiempo();
            jLabel1.setText(h+":"+m+":"+s+" "+xm);
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e){
                
            }
        }
    }

    private void tiempo(){
        Calendar reloj = new GregorianCalendar();
        Date actual = new Date();
        reloj.setTime(actual);
        xm = reloj.get(Calendar.AM_PM)== Calendar.AM?"AM":"PM";
        
        if(xm.equals("PM")){
            int ch = reloj.get(Calendar.HOUR_OF_DAY)-12;
            h = ch>9?""+ch:"0"+ch;
        }else{
            h = reloj.get(Calendar.HOUR_OF_DAY)>9?""+reloj.get(Calendar.HOUR_OF_DAY):"0"+reloj.get(Calendar.HOUR_OF_DAY);
        }
        m = reloj.get(Calendar.MINUTE)>9?""+reloj.get(Calendar.MINUTE):"0"+reloj.get(Calendar.MINUTE);
        s = reloj.get(Calendar.SECOND)>9?""+reloj.get(Calendar.SECOND):"0"+reloj.get(Calendar.SECOND);
    }
}
